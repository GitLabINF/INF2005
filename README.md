Cours INF2005 Programmation Web
===============================

Répertoire de ressources et d'exemples pour le cours.

## Pourquoi utiliser Git

Git est largement utilisé comme dépôt de source avec un historique des changements. La plupart des librairies ouvertes JavaScript ont un dépôt Git public. Connaitre les bases de Git est un atout qui vous permet de développer une application Web efficacement en équipe. L'utilisation de Git est volontaire et ne fera pas l'objet d'évaluation.

## Pourquoi GitLab

GitLab est utilisé ici, car il s'agit d'une plateforme plus ouverte. Le site hébergé permet aussi des dépôts privés gratuits. Il existe s'autres sites semblables, dont [Github](https://github.com/) et [BitBucket](https://bitbucket.org/).

## Accéder au contenu git

1. [Installez un client git](https://git-scm.com/)
2. Téléchargez une nouvelle copie du dépôt. Dans une console, exécutez:
    ```
    git clone https://gitlab.com/fgiraldeau/INF2005.git
    ```
3. Pour mettre à jour les exemples, exécutez:
    ```
    cd INF2005
    git pull
    ```
Tous les fichiers seront synchronisés à la version la plus récente.

## Accéder au Wiki

  Le [wiki du cours](https://gitlab.com/fgiraldeau/INF2005/wikis/home) contient d'autres ressources pertinentes. Pour y accéder, cliquez sur le lien Wiki dans le menu de gauche.

## Licence

Le contenu de ce dépôt est couvert par la licence [Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

<img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png"/>
