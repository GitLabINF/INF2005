INF2005 - Travail pratique 1
============================

## Mise en situation

Un nouveau restaurant de sushi vient d'ouvrir dans votre quartier. Le propriétaire de Sushi++ vous demande votre aide pour qu'il puisse accepter les commandes en ligne.

## Spécifications

Vous devez réaliser la page principale du site permettant de passer une commande. La page doit contenir le logo du restaurant, les coordonnées et le formulaire de commande. Les coordonnées du restaurant sont:

* 2020 rue Futomaki, Montréal (QC) J1J K2K
* Téléphone: 514-123-4567

Le menu du restaurant est le suivant:

* Maki californien 6$
* Maki boston 7$
* Sashimi saumon 4$
* Nigris avocat 3$
* Sushi omelette 2$

Le formulaire doit permettre de choisir la quantité de sushi de chaque type. Le client doit aussi indiquer son nom et son numéro de téléphone. Lors de la soumission de la commande, le formulaire doit être validé pour les éléments suivants:

* Le nom comporte au moins 2 caractères
* Le numéro de téléphone comporte 10 chiffres
* Il y a au moins un sushi commandé
* Le nombre de sushis ne peut pas être négatif

Si une des conditions n'est pas remplie, alors l'utilisateur reçoit un message d'erreur lui indiquant les corrections à apporter, mais il ne doit pas perdre les informations qu'il vient de saisir. Lorsque le formulaire est valide, l'utilisateur reçoit un message lui indiquant que sa commande est acceptée et en préparation. Le prix total de la facture s'affice sur la page de confirmation. La commande est enregistrée dans un fichier texte avec la date et l'heure de la commande.

## Réalisation

* Le script PHP doit valider les champs du formulaire et calculer le total de la facture.
* La soumission du formulaire doit respecter la convention POST-REDIRECT-GET.
* Toutes les commandes sont sauvegardées dans un fichier texte de type CSV. Chaque ligne représente une commande et chaque champ est séparé par un point-virgule `;`.
* Utilisez la librairie Semantic-UI pour produire des pages attrayantes. Vous pouvez vous baser sur l'exemple `html/10-ressources`. Vous pouvez utiliser une autre librairie, tel que Bootstrap.
* La page doit contenir le logo du restaurant et une image pour chaque type de sushi. Utilisez les images fournies.

## Pondération

| Élément | Points |
|--       |--      |
| Le formulaire possède les champs requis | 4 |
| Le total de la facture s'affiche correctement | 4 |
| Un formulaire invalide est refusé et une erreur s'affiche | 4 |
| Le formulaire respecte le principe POST-REDIRECT-GET | 4 |
| L'aspect des pages est professionnel (un style est appliqué à tous les éléments) | 4 |
| Total | 20 |

## Modalités

* Le TP se fait seul ou en équipe de deux.
* Effectuez une archive ZIP contenant tout le code du site.
* Les noms et les matricules des coéquipiers doivent apparaitre dans le fichier AUTEURS.txt.
* La remise se fait par Moodle.
* Ne remettez qu'un fichier ZIP par équipe.
* La date limite de remise est le 23 février 2017 23h59.
