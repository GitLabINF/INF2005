<?php
  error_reporting(E_ALL);
  // Exemple d'erreur de syntaxe
  //variable = "il manque le prefix $";
?>

<?php

  # Types de variables
  $nom = "Bob";     # Chaine de caractères
  $age = 35;        # Entier
  $taille = 1.72;   # Point flottant
  $vivant = true;   # Booléen
  $rien = null;     # Aucune valeur

  # conversion de types

  $x = 1;
  $y = 2;
  $z = $x / $y;
  echo "<p>z=$z</p>";

  $resultat = $nom . $age;
  echo "<p>res=$resultat</p>";

  $age = "99";
  $age++;
  echo "<p>age=$age</p>";

  # Tableau (Python list, C++ std::vector, Java ArrayList)
  $couleurs = array("orange", "green", "blue");
  $orange = $couleurs[0];
  $couleurs[0] = "red";

  # Tableau (nouvelle syntaxe)
  $couleurs = ["red", "green", "blue"];

  # Tableau associatif (Python dict, C++ std::map, Java HashMap)
  $rgb = array("rouge"=>"#ff0000",
               "vert"=>"#00ff00",
               "bleu"=>"#0000ff");
  $rouge = $rgb['rouge'];
  $rgb['rouge'] = "#ee0000";

  # TODO: acceder a un indexe tableau associatif

  # Tableau associatif (nouvelle syntaxe)
  $rgb = ["rouge"=>"#ff0000",
          "vert"=>"#00ff00",
          "bleu"=>"#0000ff"];

  # Conditions
  echo "<h1>Conditions</h1>";
  $tarif = null;
  if ($age < 18) {
    $tarif = "enfant";
  } else if ($age < 65) {
    $tarif = "régulier";
  } else {
    $tarif = "senior";
  }
  echo "<p>Votre âge est $age donc votre tarif est $tarif</p>";

  # Boucles avec index
  print("<h1>Boucle avec index</h1>");
  print("<ul>");
  for ($i = 0; $i < 5; $i++) {
    echo "<li>itération $i</li>";
  }
  print("</ul>");

  # Boucles sur les valeurs d'un tableau
  print("<h1>Boucle foreach d'un tableau</h1>");
  $couleurs = array("red", "green", "blue");
  print("<ul>");
  foreach($couleurs as $couleur) {
    echo "<li style='color: $couleur'>Couleur $couleur</li>";
  }
  print("</ul>");

  # Boucle sur les éléments d'un tableau associatif
  print("<h1>Boucle foreach d'un tableau associatif</h1>");
  print("<ul>");
  foreach($rgb as $nom => $code) {
    echo "<li style='color: $code'>Couleur $nom</li>";
  }
  print("</ul>");

  # Fonction
  print("<h1>Fonction</h1>");
  function fibo($x) {
    if ($x == 0 || $x == 1)
      return $x;
    return fibo($x - 1) + fibo($x - 2);
  }
  $result = fibo(20);
  echo "<p>Calcule de fibo(10) donne $result</p>";

  # Classe et objet
  print("<h1>Classe et objet</h1>");
  # Definition d'une classe
  class Point {

    # Membres de la classe
    public $x;
    public $y;

    # Constructeur
    public function __construct($x, $y) {
      $this->x = $x;
      $this->y = $y;
    }

    # Imprimer l'objet
    public function __toString() {
      return "($this->x, $this->y)";
    }

    public function dist($other) {
      $dx = $this->x - $other->x;
      $dy = $this->y - $other->y;
      return sqrt($dx * $dx + $dy * $dy);
    }
  };

  $p1 = new Point(3, 4);
  $p2 = new Point(6, 8);
  $distance = $p1->dist($p2);
  print("<p>La distance entre $p1 et $p2 est $distance</p>");
?>
