<?php error_reporting(E_ALL); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Chargement de code et de styles</title>
    <link href="../../res/semantic.min.css" rel="stylesheet" type="text/css">
    <link href="../../res/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <script src="../../res/jquery-3.2.1.min.js"></script>
    <script src="../../res/semantic.min.js"></script>
  </head>
  <body>
    <div class="ui masthead vertical segment">
      <div class="ui container">
          <h1 class="ui header aligned center">
            FooBar nouvelles
            <div class="sub header">
              De tout pour tout le monde
            </div>
          </h1>

          <div class="ui hidden divider"></div>
        </div>

        <!-- Structure de l'élément -->
        <div class="ui centered card">
          <div class="content">
            <p>contenu</p>
          </div>
          <div class="image">
            <img class="ui wireframe image" src="../../res/images/foret.jpg">
          </div>
          <div class="content">
            <span class="right floated">
              <i class="heart outline like icon"></i>
              17 likes
            </span>
            <i class="comment icon"></i>
            3 comments
          </div>
        </div>
<?php

  # Présentation d'items d'un fil d'actualité
  # Chaque item comporte un titre, un auteur, une image et un nombre de coeur

  # Source de données
  $nouvelles = [
    ["titre"=>"Mon chat est si mignon!", "nom"=>"Alice", "image"=>"chat.jpg", "coeur"=>16361],
    ["titre"=>"Les journées rallongent", "nom"=>"Bob", "image"=>"foret.jpg", "coeur"=>321],
    ["titre"=>"Voyage en Islande (faisait frette)", "nom"=>"Yeti", "image"=>"islande.jpg", "coeur"=>42],
    ["titre"=>"Travailler, c'est trop dur", "nom"=>"Franky", "image"=>"travail.jpg", "coeur"=>0],
  ];

  # Génération du HTML

  foreach($nouvelles as $nouvelle) {
    $titre = $nouvelle["titre"];
    $nom = $nouvelle["nom"];
    $image = $nouvelle["image"];
    $coeur = $nouvelle["coeur"];

    # echo "<p>$titre $nom $image $coeur</p>";

    echo "<div class=\"ui centered card\">" .
      "<div class=\"content\">" .
      "<p>$titre</p>" .
      "</div>" .
      "<div class=\"image\">" .
      "<img class=\"ui wireframe image\" src=\"../../res/images/$image\">" .
      "</div>" .
      "<div class=\"content\">" .
      "<span class=\"right floated\">" .
      "<i class=\"heart outline like icon\"></i>" .
      "$coeur aimes" .
      "</span>" .
      "<i class=\"comment icon\"></i>" .
      "$nom" .
      "</div>" .
      "</div>";

  }

?>

    </div>
  </body>
</html>
