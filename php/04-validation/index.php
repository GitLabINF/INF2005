<?php error_reporting(E_ALL); ?>

<?php
  # Traitement du formulaire

  # Fonction pour nettoyer toutes les chaines reçues
  # Important pour éviter l'injection du code
  function sanitize($data) {
    return htmlspecialchars(stripslashes(trim($data)));
  }

  # initialisation des variables
  $nom = "";
  $accepte = false;

  $formulaireOk = false;
  $erreurNom = "";
  $erreurAccepte = "";

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    # Obtenir les valeurs depuis la requête
    $nom = sanitize($_POST["ton-nom"]);
    $accepte = array_key_exists("accepte", $_POST);

    # Valider les champs
    if (empty($nom)) {
      $erreurNom = "Yo, tu as oublié ton nom!";
    }
    if ($accepte == false) {
      $erreurAccepte = "Yo, faut que t'accepte les termes!";
    }

    $formulaireOk = !empty($nom) && $accepte;

    # On pourrait enregistrer les données valides ici
  }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <title>Chargement de code et de styles</title>
  <link href="../../res/semantic.min.css" rel="stylesheet" type="text/css">
  <link href="../../res/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <script src="../../res/jquery-3.2.1.min.js"></script>
  <script src="../../res/semantic.min.js"></script>
</head>
<body>
  <div class="ui masthead vertical segment">
    <div class="ui container">
      <h1 class="ui header aligned center">
        Validation de formulaire côté serveur
      </h1>

      <div class="ui hidden divider"></div>
    </div>
    <div class="ui main container">
      <form class="ui form" action="index.php" method="post">
        <div class="field">
          <label>Nom</label>
          <input type="text" name="ton-nom" placeholder="Nom" value="<?php echo $nom; ?>">
        </div>
        <div class="field">
          <div class="ui checkbox">
            <input name="accepte" type="checkbox" tabindex="0" <?php echo $accepte ? "checked" : ""; ?>>
            <label>J'accepte les conditions</label>
          </div>
        </div>

        <?php
        $erreur = !empty($erreurAccepte) || !empty($erreurNom);
        if ($erreur) {
          echo "<div class='ui red message'>" .
            "<div class='header'>Erreur dans le formulaire</div>";
          if ($erreurNom) {
              echo "<p>$erreurNom</p>";
          }
          if ($erreurAccepte) {
            echo "<p>$erreurAccepte</p>";
          }
          echo "</div>";
        } else if ($formulaireOk) {
          echo "<div class='ui green message'>" .
          "<div class='header'>Formulaire valide!</div>" .
          "<p>$nom</p>" .
          "<p>$accepte</p>" .
          "</div>";
        }
        ?>

        <button class="ui button" type="submit">Soumettre</button>
      </form>
    </div>
  </div>
</body>
</html>
